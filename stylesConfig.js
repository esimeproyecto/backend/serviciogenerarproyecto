const fse = require('fs-extra');
var fs = require('fs')

let index = `<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>MiProyecto</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>

<body>
    <script src="https://kit.fontawesome.com/67cb18756f.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <app-root></app-root>
</body>

</html>`

let appHTML = `<app-home></app-home>
<router-outlet></router-outlet>`;

let styles = `/* You can add global styles to this file, and also import other style files */

html,
body {
    height: 100%;
}

body {
    margin: 0;
    font-family: Roboto, "Helvetica Neue", sans-serif;
}

table {
    width: 100%;
}

.mat-form-field {
    font-size: 14px;
    width: 90%;
}

.th,
.mat-header-cell {
    color: #212529;
    font-size: 14px !important;
}

.td,
.mat-cell {
    color: #212529;
    font-size: 12px;
}

.mat-row:nth-child(even) {
    background-color: #ffff;
}

.mat-row:nth-child(odd) {
    background-color: #F6F6F6
}

a:hover {
    text-decoration: underline;
}

.fa-trash-alt:hover {
    color: red;
}

.espacio-relleno {
    display: inline-block;
    padding: 10px;
}

.valid-message {
    background: #2ECC71;
    color: white;
}

.invalid-message {
    background: #E74C3C;
    color: white;
}`


const generateFilesIndex = (path) => {
    fse.outputFile(`${path}/MiProyecto/src/index.html`, index)
        .catch(err => {
            return err;
        });
    fse.outputFile(`${path}/MiProyecto/src/app/app.component.html`, appHTML)
        .catch(err => {
            return err;
        });
    fse.outputFile(`${path}/MiProyecto/src/styles.css`, styles)
        .catch(err => {
            return err;
        });

}

exports.generateFilesIndex = generateFilesIndex;