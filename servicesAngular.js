const fse = require('fs-extra');

let httpInterceptor = `import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpResponse } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { HttpEvent } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { SpinnerService } from './spinner-service.service';

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {

  constructor(private spinnerService: SpinnerService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

      this.spinnerService.show();

      return next
          .handle(req)
          .pipe(
              tap((event: HttpEvent<any>) => {
                  if (event instanceof HttpResponse) {
                      this.spinnerService.hide();
                  }
              }, (error) => {
                  this.spinnerService.hide();
              })
          );
  }
}`;

let spinner = `import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  visibility: BehaviorSubject<boolean>;
  constructor() {
    this.visibility = new BehaviorSubject(false);
  }
  show() {
    this.visibility.next(true);
  }
  hide() {
    this.visibility.next(false);
  }
}
`;


function writeFile(path, contents) {
    mkdirp(getDirName(path), function(err) {
        if (err) {
            console.log(err);
        };

        fs.writeFile(path, contents, cb);
    });
}

const makeServices = async path => {

    fse.outputFile(`${path}/MiProyecto/src/app/services/http-interceptor.ts`, httpInterceptor)
        .catch(err => {
            return err;
        });
    fse.outputFile(`${path}/MiProyecto/src/app/services/spinner-service.service.ts`, spinner)
        .catch(err => {
            return err;
        });
}


exports.makeServices = makeServices;