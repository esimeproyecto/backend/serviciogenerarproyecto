const fse = require('fs-extra');

let firstPart;
let secondPart;
let thirdPart;

function initialize() {
    firstPart =
        `import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { CustomHttpInterceptor } from './services/http-interceptor';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatRadioModule } from '@angular/material/radio';
import { MatStepperModule} from '@angular/material/stepper';
import { MatIconModule} from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatNativeDateModule } from '@angular/material/core';
import { MatBadgeModule } from '@angular/material/badge'; \n`;
    thirdPart = `],
imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    MatNativeDateModule,
    MatDividerModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatBadgeModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: CustomHttpInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }`;

}




function generateImportsDeclarations(tableNames) {
    let folder = ["Search", "Add", "Update"];
    let imports = '';
    for (let j = 0; j < tableNames.length; j++) {
        for (let i = 0; i < 3; i++) {
            let newName = tableNames[j].toUpperCase() + folder[i];
            imports += `import { ${newName + 'Component' } } from './components/${tableNames[j].toLowerCase()}/${folder[i]}/${newName.toLowerCase()}.component'; \n`
        }
    }

    imports += `\n @NgModule({\ndeclarations: [\nAppComponent,\nHomeComponent,\n`;

    for (let j = 0; j < tableNames.length; j++) {
        for (let i = 0; i < 3; i++) {
            let newName = tableNames[j].toUpperCase() + folder[i];
            imports += `${newName}Component, \n`
        }
    }

    secondPart = imports;

}

const generateTemplateModulos = (data, path) => {
    initialize();
    let tableNames = [];
    let fullTemplate;
    for (let i = 0; i < data.length; i++) {
        data[i].table_name = data[i].table_name.replace(/\s+/g, '');
        data[i].table_name = data[i].table_name.replace(/[^a-zA-Z0-9]/g, '');
        tableNames[i] = data[i].table_name;
    };
    generateImportsDeclarations(tableNames);
    fullTemplate = firstPart + secondPart + thirdPart;
    fse.outputFile(`${path}/MiProyecto/src/app/app.module.ts`, fullTemplate)
        .catch(err => {
            return err;
        });

}


exports.generateTemplateModulos = generateTemplateModulos;