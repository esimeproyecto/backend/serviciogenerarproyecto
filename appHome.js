const fse = require('fs-extra');
var fs = require('fs');
let firstPart;
let lastPart;
let homeTS;

function initialize() {
    firstPart = `<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #E74C3C;">
    <a class="navbar-brand" href="#">
        <img src="https://upload.wikimedia.org/wikipedia/commons/a/ab/Android_O_Preview_Logo.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy"> MiEmpresa
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">\n`

    lastPart = `\n        </ul>
    </div>
    </nav>`;

    homeTS = `import { Component, OnInit } from '@angular/core';
    
    @Component({
      selector: 'app-home',
      templateUrl: './home.component.html',
      styleUrls: ['./home.component.css']
    })
    export class HomeComponent implements OnInit {
    
      constructor() { }
    
      ngOnInit(): void {
      }
    
    }`;
}



function generarTabs(data) {
    data.forEach(tabla => {
        firstPart += `\n<li class="nav-item active">
            <a class="nav-link" [routerLink]="['${tabla.toUpperCase()+ 'Search'}']"> ${tabla.toUpperCase()} <span class="sr-only">(current)</span></a>
        </li>\n`
    });

}


const generateHome = (data, path) => {
    initialize();
    fse.outputFile(`${path}/MiProyecto/src/app/components/home/home.component.ts`, homeTS)
        .catch(err => {
            return err;
        });
    fse.outputFile(`${path}/MiProyecto/src/app/components/home/home.component.css`, '')
        .catch(err => {
            return err;
        });
    let tableNames = [];
    let fullTemplate;
    for (let i = 0; i < data.length; i++) {
        data[i].table_name = data[i].table_name.replace(/\s+/g, '');
        data[i].table_name = data[i].table_name.replace(/[^a-zA-Z0-9]/g, '');
        tableNames[i] = data[i].table_name;
    };
    generarTabs(tableNames);

    fullTemplate = firstPart + lastPart;
    fse.outputFile(`${path}/MiProyecto/src/app/components/home/home.component.html`, fullTemplate)
        .catch(err => {
            return err;
        });
}

exports.generateHome = generateHome;