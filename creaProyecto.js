const util = require('util');
const exec = util.promisify(require('child_process').exec);

const generateProject = async(name, folderName) => {
    const { stdout, stderr } = await exec(`mkdir -p ./ProyectosGenerados/${folderName} && cd ./ProyectosGenerados/${folderName} && ng new ${name} --skip-install true --routing=true --style=css  `);
     // --skip-install true
     return true;
}



exports.generateProject = generateProject;