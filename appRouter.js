const fse = require('fs-extra');
let firstPart;
let lastPart;

function initialize() {
    firstPart = `import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';\n`
    lastPart = `\n@NgModule({
  imports: [RouterModule.forRoot(routes , { useHash: true} )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
`;
}




function generarRoutes(data) {
    let folder = ["Search", "Add", "Update"];
    data.forEach(tabla => {
        for (let i = 0; i < 3; i++) {
            let newName = tabla.toUpperCase() + folder[i];
            firstPart += `import { ${newName + 'Component' } } from './components/${tabla.toLowerCase()}/${folder[i]}/${newName.toLowerCase()}.component'; \n`
        }
    });
    firstPart += `\nconst routes: Routes = [\n`
    data.forEach(tabla => {
        for (let i = 0; i < 3; i++) {
            let newName = tabla.toUpperCase() + folder[i];
            firstPart += `{ path: '${newName}', component: ${newName + 'Component' } },\n`
        }
    });
    firstPart += `{ path: '**', pathMatch: 'full' , redirectTo: 'tabs' }];\n`
}


const generateRouter = (data, path) => {
    initialize();
    let tableNames = [];
    let fullTemplate;
    for (let i = 0; i < data.length; i++) {
        data[i].table_name = data[i].table_name.replace(/\s+/g, '');
        data[i].table_name = data[i].table_name.replace(/[^a-zA-Z0-9]/g, '');
        tableNames[i] = data[i].table_name;
    };
    generarRoutes(tableNames);
    fullTemplate = firstPart + lastPart;
    fse.outputFile(`${path}/MiProyecto/src/app/app-routing.module.ts`, fullTemplate)
        .catch(err => {
            return err;
        });
}

exports.generateRouter = generateRouter;