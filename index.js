const express = require('express');
const app = express();
const { uuid } = require('uuidv4');
const bodyParser = require('body-parser');
const proyecto = require('./creaProyecto');
const services = require('./servicesAngular');
const appModulos = require('./AppModulos');
const appRouter = require('./appRouter');
const appHome = require('./appHome');
const appStyles = require('./stylesConfig')
const fs = require('fs');
const path = require('path');
var cors = require('cors');
const { exit } = require('process');

const port = process.env.PORT || 3000;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.post('/generarProyecto', async function(req, res) {
    console.log("entro");
    let data = req.body;
    let folderName = uuid(); //declara nombre unico
    try {
        //crea folder con nombre unico
        // await fs.mkdir(`./ProyectosGenerados/${folderName}`, { recursive: true }, err => {});
        //genera proyecto angular en el folder nuevo
        await proyecto.generateProject("MiProyecto", folderName);
        //consigue la ruta del archivo
        var absolutPath = path.resolve(`./ProyectosGenerados/${folderName}`);
        // crea servicios httpinterceptor y spinner
        await services.makeServices(absolutPath);
        // crea el archivo donde se declaran los modulos
        await appModulos.generateTemplateModulos(data, absolutPath);
        // crea el archivo dodde se declaran las rutas
        await appRouter.generateRouter(data, absolutPath);
        // crea el home
        await appHome.generateHome(data, absolutPath);
        // configura archivos
        await appStyles.generateFilesIndex(absolutPath);
    } catch (err) {
        res.json({ status: false });
        throw console.log(err);
    }
    res.json({ status: true, file: absolutPath });
});

app.listen(port, function() {
    console.log('El sitio de APIs inició correctamente en el puerto: ', port);
});